#include "stdafx.h"
#include <Windows.h>
#include <TlHelp32.h>
#include <tchar.h>
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

void PrintError(const string& msg)
{
	DWORD eNum;
	TCHAR sysMsg[256];
	TCHAR* p;

	eNum = GetLastError();
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, eNum,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		sysMsg, 256, NULL);
	p = sysMsg;
	while ((*p > 31) || (*p == 9))
		++p;
	do { *p-- = 0; } while ((p >= sysMsg) &&
		((*p == '.') || (*p < 33)));
	_tprintf(TEXT("\n  WARNING: %s failed with error %d (%s)\n"), msg.c_str(), eNum, sysMsg);
}

bool GetProcessPidByName(const LPTSTR& processName, DWORD& pid)
{
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		PrintError(TEXT("CreateToolhelp32Snapshot (of processes)"));
	}
	else
	{
		pe32.dwSize = sizeof(PROCESSENTRY32);
		if (!Process32First(hProcessSnap, &pe32))
		{
			PrintError(TEXT("Process32First")); // show cause of failure
		}
		else
		{
			do
			{
				if (!_tcscmp(pe32.szExeFile, processName))
				{
					CloseHandle(hProcessSnap);
					pid = pe32.th32ProcessID;
					return(true);
				}
			} while (Process32Next(hProcessSnap, &pe32));
		}

	}
	CloseHandle(hProcessSnap);
	return(false);
}

UINT_PTR GetModuleBase(const LPTSTR& lpModuleName, const DWORD& dwProcessId)
{
	MODULEENTRY32 lpModuleEntry = { 0 };
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessId);
	if (!hSnapShot)
		return NULL;
	lpModuleEntry.dwSize = sizeof(lpModuleEntry);
	BOOL bModule = Module32First(hSnapShot, &lpModuleEntry);
	while (bModule)
	{
		if (!_tcscmp(lpModuleEntry.szModule, lpModuleName))
		{
			CloseHandle(hSnapShot);
			return (UINT_PTR)lpModuleEntry.modBaseAddr;
		}
		bModule = Module32Next(hSnapShot, &lpModuleEntry);
	}
	CloseHandle(hSnapShot);
	return NULL;
}

UINT_PTR DeRef(const UINT_PTR& _uiptrPointer, const HANDLE& g_hProcess)
{
	UINT_PTR uiptrRet;
	if (!ReadProcessMemory(g_hProcess,
		reinterpret_cast<LPVOID>(_uiptrPointer),
		&uiptrRet,
		sizeof(uiptrRet), NULL))
	{
		return 0UL;
	}
	return uiptrRet;
}

void PrintOut(const string& message)
{
	cout << message << endl;
}

void PrintHead(const bool& started=true)
{
	system("CLS");
	PrintOut("CheatMeNot16 BETA5  http://bitbucket.org/dontcopythatfloppy/cheatmenot16-beta");
	PrintOut("=============================================================================");
	started ? PrintOut("Detecting opponent..\n") : PrintOut("Waiting for FIFA16 to be launched..\n");
}

enum listType {
	WHITELIST = 0,
	BLACKLIST = 1
};

void ReadList(vector<string>& list, listType type)
{
	ifstream t;
	string name;
	list.clear();

	type == WHITELIST ? t.open("white.txt") : t.open("black.txt");
	if (t.is_open())
	{
		while (!t.eof())
		{
			getline(t, name);
			list.push_back(name);
		}
		t.close();
	}
}

void AddToList(const string& originId, vector<string>& list, listType type) 
{
	list.push_back(originId);
	ofstream file;
	type == WHITELIST ? file.open("white.txt", ios::out | ios::trunc) : file.open("black.txt", ios::out | ios::trunc);
	if (file.is_open())
	{
		for (auto const& id : list)
		{
			file << id << endl;
		}
		file.close();
	}
}

bool InList(const string& originId, const vector<string>& list) 
{
	return std::find(list.begin(), list.end(), originId) != list.end();
}

string ExePath() 
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer).substr(0, pos);
}

void OpenBrowser(const string& originId, const int& mode) {
	ofstream fileout;
	fileout.open("bounce.html");
	fileout << "<html><head><meta http-equiv=\"refresh\" content=\"0; URL = 'frames.html?originId=" + originId + "'\"></head></html>";
	fileout.close();
	string bounce = "file:///" + ExePath() + "\\bounce.html";
	LPCSTR wurl = bounce.c_str();
	ShellExecute(NULL, NULL, wurl, NULL, NULL, mode);
}

int _tmain(int argc, _TCHAR* argv[])
{
	const int BASE_OFFSET[] = { 0x34ECCD0 };
	const int PLAYER_OFFSET0[] = { 0x318 };
	const int PLAYER_OFFSET1[] = { 0x198 };
	const int PLAYER_OFFSET2[] = { 0x770 };
	
	vector<string> blackList;
	vector<string> whiteList;

	ReadList(blackList, BLACKLIST);
	ReadList(whiteList, WHITELIST);

	int usedPath = 0;
	int pathToUse = -1;

	DWORD pid = 0;
	string url = "";
	
	bool openurl = true;
	
	for (int a = 0; a < argc; ++a)
	{
		if (!_tcscmp(argv[a], "--disable-auto-open-profile")) 
		{
			openurl = false;
		}
		if (!_tcscmp(argv[a], "--use-path"))
		{
			if ((a + 1) < argc)
			{
				pathToUse = std::stoi(argv[a + 1]) - 1;
			}
		}
	}
	bool fifaIsRunning = GetProcessPidByName("fifa16.exe", pid);
	if (!fifaIsRunning)
	{
		while (!fifaIsRunning)
		{
			PrintHead(false);
			Sleep(3000);
			fifaIsRunning = GetProcessPidByName("fifa16.exe", pid);
		}
		PrintOut("\tFIFA16.exe is now running, waiting for 30 seconds to let it initialize its memory..");
		Sleep(30000);
	}
	UINT_PTR baseAddress = GetModuleBase("fifa16.exe", pid);
	if (baseAddress != NULL)
	{
		char memBuffer[17];
		UINT_PTR ptrOriginId;
		HANDLE g_hProcess;

		g_hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

		while (pathToUse != -1 || usedPath < sizeof(BASE_OFFSET) && GetProcessPidByName("fifa16.exe", pid))
		{
			if (pathToUse != -1)
			{
				usedPath = pathToUse;
			}
			ptrOriginId = DeRef(DeRef(DeRef(baseAddress + BASE_OFFSET[usedPath], g_hProcess) + PLAYER_OFFSET0[usedPath], g_hProcess) + PLAYER_OFFSET1[usedPath], g_hProcess) + PLAYER_OFFSET2[usedPath];
			if (ReadProcessMemory(g_hProcess, (LPVOID)(ptrOriginId), &memBuffer, sizeof(memBuffer), NULL) != 0) 
			{
				bool running = true;
				bool got_newname = false;
				bool doneFirstRead = false;
				bool listed = false;

				ATOM hk_black = GlobalAddAtom("CheatMeNot16_BlacklistPlayer");
				ATOM hk_white = GlobalAddAtom("CheatMeNot16_WhitelistPlayer");
				ATOM hk_profile = GlobalAddAtom("CheatMeNot16_OpenPlayerProfile");

				if (!RegisterHotKey(NULL, hk_black, MOD_ALT | MOD_NOREPEAT, 'B') ||
					!RegisterHotKey(NULL, hk_white, MOD_ALT | MOD_NOREPEAT, 'W') ||
					!RegisterHotKey(NULL, hk_profile, MOD_ALT | MOD_NOREPEAT, 'C'))
				{
					if (!RegisterHotKey(NULL, hk_black, MOD_ALT, 'B') ||
						!RegisterHotKey(NULL, hk_white, MOD_ALT, 'W') ||
						!RegisterHotKey(NULL, hk_profile, MOD_ALT, 'C'))
					{
						PrintError(TEXT("Couldn't register hotkeys!"));
					}
					
				}

				MSG msg;
				string originId;
				originId.assign(memBuffer);

				PrintHead();

				while (running)
				{
					ptrOriginId = DeRef(DeRef(DeRef(baseAddress + BASE_OFFSET[usedPath], g_hProcess) + PLAYER_OFFSET0[usedPath], g_hProcess) + PLAYER_OFFSET1[usedPath], g_hProcess) + PLAYER_OFFSET2[usedPath];
					if (ReadProcessMemory(g_hProcess, (LPVOID)(ptrOriginId), &memBuffer, sizeof(memBuffer), NULL) != 0) 
					{
						if (originId.compare(memBuffer) != 0) 
						{
							originId.assign(memBuffer);
							doneFirstRead = true; // flag that signals that we read at least one vaild name so we can eventually open the profile manually if the user wants
							got_newname = true;
						}

						if (got_newname) 
						{
							PrintHead();
							openurl ? PrintOut("\tOriginID: " + originId + ". (FUT profile opened in the browser, ALT+C to reopen.)") : PrintOut("\tOriginID: " + originId + ". (ALT+C opens FUT profile in the browser.)");
							
							
							got_newname = false;
							listed = false;
							if (InList(originId, blackList))
							{
								PrintOut("\n\t\tBLACKLISTED OPPONENT! Back off or risk it.");
								listed = true;
							}
							if (InList(originId, whiteList))
							{
								PrintOut("\n\t\tWHITELISTED OPPONENT! <3");
								listed = true;
							}
							if (!listed)
							{
								// open browser automatically
								if (openurl)
								{
									OpenBrowser(originId, SW_SHOWNOACTIVATE);
								}
								PrintOut("\n\tALT+b to blacklist, ALT+w to whitelist or just do nothing if you're unsure.");
							}							
						}
					}
					else 
					{
						running = false;
						PrintError(TEXT("Couldn't read name memory address! Is FIFA 16 still running? Path used: #" + to_string(usedPath)));
					}

					// input
					if (MsgWaitForMultipleObjects(0, NULL, FALSE, 1000, QS_ALLEVENTS) == WAIT_OBJECT_0) 
					{
						while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) 
						{
							if (doneFirstRead)
							{
								if (msg.message == WM_HOTKEY && msg.wParam == hk_profile)
								{
									OpenBrowser(originId, SW_SHOW);
								}
								if (!listed) {
									if (msg.message == WM_HOTKEY && msg.wParam == hk_black) {
										if (!InList(originId, blackList))
										{
											AddToList(originId, blackList, BLACKLIST);
											PrintOut("\n\tOpponent blacklisted.");
											listed = true;
										} 
									}
									if (msg.message == WM_HOTKEY && msg.wParam == hk_white) {
										if (!InList(originId, blackList))
										{
											AddToList(originId, whiteList, WHITELIST);
											PrintOut("\n\tOpponent whitelisted.");
											listed = true;
										}
									}
								}
							}
						}


					}
				}
			}
			else 
			{
				PrintError(TEXT("Couldn't read name memory address with path #" + to_string(usedPath)));
				/*
				if (pathToUse == -1)
				{
					if (usedPath < 5)
					{
						usedPath++;
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
				*/
				break;
			}
		}
	}
	else
	{
		PrintError(TEXT("Couldn't retrieve FIFA16.exe module base address."));
	}
	system("pause");
	return 0;
}